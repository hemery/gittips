# Connaitre l'états des fichiers
```
git status
```
![git-status](git-status.mp4)
# Ajouter un fichier
```
git add [fichier_quelconque]
```
# Commiter ses modifications
```
git commit
```
# Afficher l'historique des modifications sur une seule ligne
```
git log --pretty=oneline
```
# Afficher l'historique des modifications complete
```
git log
```
# Affiche les lignes modifiées dans les fichiers après le dernier commit
```
git diff
```
# Affiche les statistiques des lignes dernièrement modifiées
```
git diff --stat
```
# Affiche les statistiques général des fichier modifié
```
git diff --shortstat
```
# Affiche les différences pas par lignes mais par mots
```
git diff --word-diff
```
# Sélectionner les modifications à commiter
```
git add -p
```
# Annuler le dernier commit conserver les modifications
```
git reset HEAD^
```
# Editer les modifications à commiter
```
git add -e
```